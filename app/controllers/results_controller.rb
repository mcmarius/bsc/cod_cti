# frozen_string_literal: true

class ResultsController < ApplicationController
  FT_PATH = 'data/models/fasttext'
  FT_MODEL = 'data/models/fastText/n_model_optimism_356.ftz'
  WARM_COLORS = %w[success bright love sunny].freeze
  COOL_COLORS = %w[sad depressed dark alone].freeze
  API_KEY = '563492ad6f91700001000001e29b5e6b863b4762a9983d255b023b6a'
  BLACKLIST_URLS = %w[
    https://images.pexels.com/photos/6505/couple-love-bedroom-kissing.jpg
    https://images.pexels.com/photos/576494/pexels-photo-576494.jpeg
    https://images.pexels.com/photos/2282848/pexels-photo-2282848.jpeg
  ].freeze

  before_action :setup_params

  def results
    if params[:model] == 'bert'
      if Rails.env.development?
      # if Rails.env.test?
        @status = system(python_command)
        @prediction_label = python_accuracy if status
      elsif session[:job_id]
        @job_id = session[:job_id]
        complete_job
      elsif params[:input]
        @status = nil
        background_prediction
      end
    elsif params[:model] == 'fasttext'
      session[:job_id] = nil
      session[:job_status] = nil
      @status = 1
      @prediction_label = fast_text
    end
    background_image if @status

    respond_to do |format|
      format.js { render layout: false } # Add this line to you respond_to block
    end
  end

  def reset
    session[:job_id] = nil
    session[:job_status] = nil
    session[:no_bg] = nil
    redirect_to '/'
  end

  def no_bg
    session[:no_bg] = true
    redirect_to '/'
  end

  private

  def setup_params
    @input = params[:input]&.squish
  end

  def python_command
    @python3_command ||= Rails.env.development? ? '/home/marius/anaconda3/envs/py36/bin/python' : 'python3'
    File.write('data/pred.tsv', "1\t#{@input}")
    @python_command ||= <<~PYTHON_COMMAND.squish
      #{@python3_command} run_classifier.py
      --bert_model data/models/bert/ --do_lower_case --do_eval --do_test --do_pred
      --output_dir results/rails_base_f_01_test_48 --data_dir data/ --task_name opt
      --train_batch_size 24 --eval_batch_size 1 --no_cuda
    PYTHON_COMMAND
  end

  def python_accuracy
    bert_output = File.read 'results/rails_base_f_01_test_48/eval_results.txt'
    puts "bert_output: '#{bert_output[9...12]}'"
    accuracy = bert_output[9...12].to_i
    label_to_text(accuracy)
  end

  def background_prediction
    @job = BertJob.perform_later("{\"input\":\"1\\t#{@input}\"}")
    puts 'add job' + @job.job_id
    session[:job_id] = @job.job_id
  end

  def complete_job
    attitude = Prediction.find_by(job_id: session[:job_id])&.attitude
    return if attitude.nil?

    session[:job_id] = nil
    session[:job_status] = nil
    @status = 1
    @prediction_label = label_to_text(attitude ? 1 : 0)
    #  label_to_text(Sidekiq::Status.get_all(@job_id)['result'][8].to_i)
  end

  def fast_text
    @local_prefix ||= Rails.env.development? ? '_local' : ''
    fast_text_output =
      `echo "#{@input}" | #{FT_PATH}#{@local_prefix} predict #{FT_MODEL} -`
    label = fast_text_output[9].to_i
    label_to_text(label)
  end

  def label_to_text(label)
    if label == 1
      'optimist'
    else
      'pesimist'
    end
  end

  def background_image
    # @img_src = 'file:///home/marius/Pictures/loss_dropout_bad.png'
    # @img_src = 'https://images.pexels.com/photos/462030/pexels-photo-462030.jpeg'
    # @img_src = 'https://images.pexels.com/photos/325044/pexels-photo-325044.jpeg'
    if session[:no_bg]
      @img_src = 'https://images.pexels.com/photos/592753/pexels-photo-592753.jpeg'
      @author = 'Sample Author'
      @author_url = 'https://www.pexels.com/@pixabay'
      return
    end
    # binding.pry
    response = HTTParty.get(api_link, headers: api_headers)
    # uri = URI.parse(api_link)
    # http = ::Net::HTTP.new(uri.host, uri.port)
    # http.use_ssl = true
    # rsp = http.get(uri.request_uri, api_headers)
    # response = ::JSON.parse(rsp.body)
    puts <<~REMAINING
      Requests remaining this month: #{response.headers['X-Ratelimit-Remaining']}\n\n
    REMAINING

    height = 720
    width = 1280

    photo_url = response['photos'][0]['src']['original']

    if photo_url.in? BLACKLIST_URLS
      if @prediction_label == 'optimist'
        @img_src = 'https://images.pexels.com/photos/3768/sky-sunny-clouds-cloudy.jpg'
        @author = 'Skitterphoto'
        @author_url = 'https://www.pexels.com/@skitterphoto'
      else
        @img_src = 'https://images.pexels.com/photos/278312/pexels-photo-278312.jpeg'
        @author = 'Pixabay'
        @author_url = 'https://www.pexels.com/@pixabay'
      end
    else
      @img_src = response['photos'][0]['src']['original']
      @author = response['photos'][0]['photographer']
      @author_url = response['photos'][0]['photographer_url']
    end
    @img_src += "?auto=compress&cs=tinysrgb&fit=crop&h=#{height}&w=#{width}"
  end

  def api_link
    color = random_color(@prediction_label)
    rand_page = (1...50).to_a.sample
    <<~API_LINK.squish
      https://api.pexels.com/v1/search?query=#{color}&per_page=1&page=#{rand_page}"
    API_LINK
  end

  def api_headers
    { 'Authorization': API_KEY }
  end

  def random_color(label)
    if label == 'optimist'
      warm_color
    else
      cool_color
    end
  end

  def warm_color
    WARM_COLORS.sample
  end

  def cool_color
    COOL_COLORS.sample
  end
end
