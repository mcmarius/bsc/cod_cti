class BertJob < ApplicationJob
  queue_as :default

  # include ActiveJobStatus::Hooks
  # include Sidekiq::Worker
  # include Sidekiq::Status::Worker
  # sidekiq_options retry: false

  # include SuckerPunch::Job
  # max_jobs 10

  API_KEY = 'simX566RLYMLlvvGGB2ByW1HybQ1'

  def perform(input_str)
    prediction = Prediction.create(input: input_str, job_id: job_id)

    # sleep 15

    # result = { "acc": 0, "eval_loss": 4.999517440795898, "global_step": 0, "loss": nil }

    # # input = "{\"input\":\"1\\t#{input_str}\"}"
    client = Algorithmia.client(API_KEY)
    algo = client.algo('mcmarius/attitude/0.1.1')
    algo.set('timeout': 400) # optional
    result = algo.pipe(input_str).result
    puts 'Received: ' + result.inspect
    # store result: result

    prediction.attitude = 1 == result['acc'].to_i
    prediction.save
    # Prediction.find_by(job_id: job_id).update_attributes(attitude: attitude)
  end

  def destroy_failed_jobs?
    false
  end
end

