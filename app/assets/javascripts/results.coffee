# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->
  $('#cover').hide()
  return

$(document).ajaxStart ->
  $('#viewing-container').hide()
  $('#cover').fadeIn 400
  return

$(document).ajaxStop ->
  $('#cover').fadeOut(500)
  $('#viewing-container').fadeIn(500)
  $('#bg-img').fadeIn(1500)
  return

$(document).ready ->
  _originalSize = $(window).width() + $(window).height()
  $(window).resize ->
    if $(window).width() + $(window).height() != _originalSize
      $('footer.files').hide()
    else
      $('footer.files').show()
    return
  return
