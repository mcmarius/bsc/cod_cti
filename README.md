# Codul sursă pentru lucrarea de licență despre clasificatori de atitudine

Cerințe: [Python](https://www.python.org/) 3.6 și [conda](https://www.anaconda.com/distribution/).

Pachetele necesare se instalează cu ajutorul `requirements.txt` (`conda` sau `pip`).

Pentru inferență, nu este necesară utilizarea GPU.

## Setul de date

Conform termenilor și condițiilor Twitter, datele nu pot fi făcute publice. Momentan, ele sunt disponibile la adresa http://web.eecs.umich.edu/~mihalcea/downloads/optimism-twitter-data.zip

Din arhivă, de interes este fișierul `tweets_annotation.csv`. Pentru procesarea pe sisteme Unix care nu sunt de tip macOS(X), trebuie eliminate caracterele `CR`:

`sed 's/\r//g' tweets_annotation.csv > tweets_annotation_compact.csv`

Am primit împărțirea datelor în train/dev/test din Caragea et al (2018), însă etichetele nu se potrivesc întocmai cu descrierea din articol.

Pentru aceasta, am scris și folosit script-ul `data/fix_labels.rb` (este necesar Ruby cu biblioteca ActiveSupport).

Pentru a transforma corpus-ul în formatul corespunzător fiecărui model, se folosesc script-urile `data/pre_to_ft` și `data/ft_to_tsv`.

## Modelul BERT

Pentru antrenare, script-ul `run_classifier.py` a fost rulat astfel:

`python run_classifier.py --bert_model bert-base-uncased --do_lower_case --do_train --output_dir data/models/bert/ --data_dir data/ --task_name opt --train_batch_size 24 --eval_batch_size 256`

Evaluarea pe setul de dev s-a făcut astfel:

`python run_classifier.py --bert_model data/models/bert/ --do_lower_case --do_eval --output_dir results/bert_base_eval_48 --data_dir data/ --task_name opt --train_batch_size 24 --eval_batch_size 256`

Evaluarea pe setul de test s-a făcut astfel:

`python run_classifier.py --bert_model data/models/bert/ --do_lower_case --do_eval --do_test --output_dir results/bert_base_test_48 --data_dir data/ --task_name opt --train_batch_size 24 --eval_batch_size 256`

## Modelul fastText

Este recomandat ca [fastText](https://github.com/facebookresearch/fastText) să fie compilat din sursă.

În continuare, se va presupune că există un alias pentru executabilul `fasttext` sau că executabilul este disponibil în variabila `$PATH`

Antrenare:

`fasttext supervised -input data/fte_train.txt -output data/models/fastText/n_model_optimism_356 -epoch 3 -lr 1.0 -wordNgrams 5 -minn 2 -maxn 6 -thread 1 -loss hs -ws 7 -dim 16`

Evaluare și testare:

`fasttext test-label data/models/fastText/n_model_optimism_356.bin data/fte_dev.txt`

`fasttext test-label data/models/fastText/n_model_optimism_356.bin data/fte_test.txt`

Inferență:

`echo 'i am really happy' | fasttext predict data/models/fastText/n_model_optimism_356.bin -`

Micșorarea modelului:

`fasttext quantize -input data/fte_train.txt -output data/models/fastText/n_model_optimism_356 -qnorm -cutoff 50000 -retrain`

Testarea modelului micșorat:

`fasttext test-label data/models/fastText/n_model_optimism_356.ftz data/fte_test.txt`

Se va afișa următorul mesaj:

```
F1-Score : 0.811146  Precision : 0.775148  Recall : 0.850649   __label__1
F1-Score : 0.651429  Precision : 0.712500  Recall : 0.600000   __label__0
N	747
P@1	0.755
R@1	0.755
```

Calcularea acurateței se realizează cu ajutorul script-ului `fast_text_exp.rb`, unde argumentele sunt precizia și recall-ul pentru eticheta 1:

`ruby fast_text_exp.rb 0.775148 0.850649`

## Server

Pentru interfață, este folosită o aplicație simplă de [Rails](https://rubyonrails.org/), cu o bază de date [PostgreSQL](https://www.postgresql.org/) pentru procese de fundal.

Este nevoie de [Ruby](https://www.ruby-lang.org/en/) minim 2.5.1 și [Bundler](https://bundler.io/) (`gem install bundler`).

Celelalte dependențe sunt [jQuery](https://jquery.com/) și [Bootstrap](https://getbootstrap.com/).

`bundle`

`rails db:setup`

`rails server`
