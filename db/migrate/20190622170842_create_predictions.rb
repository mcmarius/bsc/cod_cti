class CreatePredictions < ActiveRecord::Migration[5.2]
  def change
    create_table :predictions do |t|
      t.string 'input', null: false
      t.boolean 'attitude'
      t.string 'job_id', null: false, index: true

      t.timestamps
    end
  end
end
