# fastText

# `fasttext supervised -input fte_train.txt -output n_model_optimism_3 -epoch 3 -lr 1.0 -wordNgrams 2 -minn 2 -maxn 4 -thread 1 -loss hs -ws 7 -dim 16`

# `fasttext test-label n_model_optimism_3.bin fte_test.txt`

###############################################################################
#                                                                             #
#  F1-Score : 0.814070  Precision : 0.759850  Recall : 0.876623   __label__1  #
#  F1-Score : 0.629259  Precision : 0.733645  Recall : 0.550877   __label__0  #
#  N	747                                                                   #
#  P@1	0.752                                                                 #
#  R@1	0.752                                                                 #
#                                                                             #
###############################################################################

# grep "__1" fte_test.txt | wc -l
# 462
# grep "__0" fte_test.txt | wc -l
# 285

precision = ARGV[0].to_f  # positive label
recall    = ARGV[1].to_f
tpfn        = 462
fptn        = 285

tp = tpfn*recall

fn = tpfn - tp

fp        = (tp / precision) - tp
fn2        = (tp / recall)    - tp
tn = fptn - fp

accuracy  = (tp + tn) / (tp + fp + tn + fn)
puts({tp: tp.round, tn: tn.round, fp: fp.round, fn: fn.round, fn2: fn2.round})
puts accuracy

# fasttext supervised -input data/fte_train.txt -output data/models/fastText/n_model_optimism_05j -epoch 5 -lr 0.8 -wordNgrams 5 -minn 2 -maxn 5 -thread 1 -dim 64 -loss hs
## {:tp=>356, :tn=>179, :fp=>106, :fn=>106}
## 0.7162293333333334
#{:tp=>393, :tn=>168, :fp=>117, :fn=>69, :fn2=>69}
#0.7510036552929498

#fasttext supervised -input fte_train.txt -output n_model_optimism_356 -epoch 3 -lr 1.0 -wordNgrams 5 -minn 2 -maxn 6 -thread 1 -loss hs -ws 7 -dim 16
#fasttext test-label data/models/fastText/n_model_optimism_356.bin data/fte_test.txt
#ruby fast_text_exp.rb 0.760223 0.885281
#{:tp=>409, :tn=>156, :fp=>129, :fn=>53, :fn2=>53}
#0.756358559493083

