Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'results/show'
  post 'results' => 'results#results'
  post 'results/results' => 'results#results'
  post 'results/show/results' => 'results#results'
  get 'reset' => 'results#reset'
  get 'no_bg' => 'results#no_bg'
  root to: 'results#show'
  get'*path' => redirect('/')
end
