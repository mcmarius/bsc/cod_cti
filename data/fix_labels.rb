require 'active_support/all'
require 'csv'

tw = CSV.read('tweets_annotation_compact.csv');
positive_tweets = tw[1..-1].sort_by(&:last).reject { |x| x.last.to_f < 0 }.deep_dup;
negative_tweets = tw[1..-1].sort_by(&:last).select { |x| x.last.to_f < 0 }.deep_dup;
positive_tweets1 = positive_tweets.deep_dup.map { |x| x[-1]=1; x };
negative_tweets1 = negative_tweets.deep_dup.map { |x| x[-1]=0; x };
twww = (positive_tweets1+negative_tweets1).shuffle;
tws = ([tw[0]]+twww);
tws.map! { |x| x[0].downcase!; x }
File.write('hard_scores.csv', tws.map{|x|x.join(',')}.join("\n"))

['train.csv', 'dev.csv', 'test.csv'].each do |f|
  ds = CSV.read(f)
  ds1 = ds.map(&:first).map(&:downcase)
  ds2 = tws.select { |x| x[0].in? ds1 }
  File.write("_#{f}", "#{ds2.map { |x| x.join(',') }.join("\n")}\n")
end

