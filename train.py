from pathlib import Path

import flair.datasets

from flair.embeddings import BytePairEmbeddings, FlairEmbeddings, DocumentPoolEmbeddings, WordEmbeddings
from flair.models import TextClassifier
from flair.trainers import ModelTrainer
from flair.visual.training_curves import Plotter

# from pytorch_pretrained_bert import OpenAIGPTLMHeadModel

# use your own data path
data_folder = Path('../../corpora/optimism_fixed/')

# load corpus containing training, test and dev data
corpus = \
    flair.datasets.ClassificationCorpus(data_folder, test_file='fte_test.txt', dev_file='fte_dev.txt',
                                        train_file='fte_train.txt', in_memory=True)
labels = corpus.make_label_dictionary()

byte_embeddings = BytePairEmbeddings('en')
twitter_embeddings = WordEmbeddings('twitter')
# doc_embeddings = DocumentPoolEmbeddings([  # byte_embeddings,
#                                          twitter_embeddings
#                                          ],
#                                         mode='mean')
flair_embedding_forward = FlairEmbeddings('news-forward', use_cache=True)
flair_embedding_backward = FlairEmbeddings('news-backward', use_cache=True)
# gpt_embeddings = flair.embeddings.OpenAIGPTEmbeddings()
doc_embeddings = DocumentPoolEmbeddings([
#    twitter_embeddings,
    byte_embeddings,
    # gpt_embeddings
#    flair_embedding_backward,
#    flair_embedding_forward
])

classifier = TextClassifier(doc_embeddings, labels)
# model = OpenAIGPTLMHeadModel.from_pretrained('openai-gpt')
# model.eval()
# model.to('cuda')

# TODO: Bert for sequence classification?

classifier = classifier.cuda()
trainer = ModelTrainer(classifier, corpus)

trainer.train('resources/classifiers/optimism_fixed2',
              learning_rate=0.3,
              mini_batch_size=4096,
              anneal_factor=0.5,
              patience=1,
              max_epochs=25)

plotter = Plotter()
plotter.plot_training_curves('resources/classifiers/optimism_fixed2/loss.tsv')
plotter.plot_weights('resources/classifiers/optimism_fixed2/weights.txt')
